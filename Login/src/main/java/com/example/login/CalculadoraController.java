package com.example.login;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class CalculadoraController {

    @FXML
    private Label pantalla;

    @FXML
    void cero(ActionEvent event) {
        digitoPantalla("0");

    }

    @FXML
    void cinco(ActionEvent event) {
        digitoPantalla("5");

    }

    @FXML
    void cuatro(ActionEvent event) {
        digitoPantalla("4");

    }

    @FXML
    void dividir(ActionEvent event) {
        operaciones("/");

    }

    @FXML
    void dos(ActionEvent event) {
        digitoPantalla("2");

    }

    @FXML
    void igual(ActionEvent event) {
        operaciones("=");

    }

    @FXML
    void mas(ActionEvent event) {
        operaciones("+");

    }

    @FXML
    void menos(ActionEvent event) {
        operaciones("-");

    }

    @FXML
    void nueve(ActionEvent event) {
        digitoPantalla("9");

    }

    @FXML
    void ocho(ActionEvent event) {
        digitoPantalla("8");

    }

    @FXML
    void por(ActionEvent event) {
        operaciones("*");

    }

    @FXML
    void punto(ActionEvent event) {
        if(!punto && !digito){
            pantalla.setText("0.");
            digito=true;
        }
        else if(!punto){
            String valActual=pantalla.getText();
            pantalla.setText(valActual+".");
        }
        punto=true;

    }

    @FXML
    void seis(ActionEvent event) {
        digitoPantalla("6");

    }

    @FXML
    void siete(ActionEvent event) {
        digitoPantalla("7");

    }

    @FXML
    void te(ActionEvent event) {
        digito=false;
        punto=false;
        numOperador=0;
        operador1=0;
        operador2=0;
        operador=' ';
        pantalla.setText("");

    }

    @FXML
    void tres(ActionEvent event) {
        digitoPantalla("3");

    }

    @FXML
    void uno(ActionEvent event) {
        digitoPantalla("1");

    }

    private boolean digito=false;
    private boolean punto=false;
    private int numOperador=0;
    private double operador1, operador2;
    private char operador=' ';

    private void digitoPantalla(String numero){

        if(!digito && numero.equals("0"))
            return;
        if(!digito){
            pantalla.setText("");
            punto=false;
        }

        String valActual=pantalla.getText();
        pantalla.setText(valActual+numero);
        digito=true;

    }
    private void operaciones(String operador){

        if(digito)
            numOperador++;

        if(numOperador==1)
            operador1=Double.parseDouble(pantalla.getText());

        if(numOperador==2){
            operador2=Double.parseDouble(pantalla.getText());
            switch (this.operador){
                case'+':
                operador1=operador1+operador2;
                break;
                case'-':
                    operador1=operador1-operador2;
                    break;
                case'/':
                    operador1=operador1/operador2;
                    break;
                case'*':
                    operador1=operador1*operador2;
                    break;
                case'=':
                    operador1=operador2;
                    break;
            }
            pantalla.setText(String.valueOf(operador1));
            numOperador=1;
            punto=false;
        }
        digito=false;
        this.operador=operador.charAt(0);
    }
}


