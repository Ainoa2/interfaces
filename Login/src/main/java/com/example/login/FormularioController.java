package com.example.login;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


import java.beans.XMLDecoder;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class FormularioController implements Initializable {
    @FXML
    private TextField label;


    @FXML
    private ChoiceBox<String> ciudad;

    private String[] ciudades = {"Valencia", "Alicante", "Castellon"};

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        ciudad.getItems().addAll(ciudades);
        sistema.getItems().addAll(sis);

    }

    @FXML
    private TextArea comentario;

    @FXML
    private TextField nombre;


    @FXML
    private ChoiceBox<String> sistema;
    private String[] sis = {"Windows", "Linux", "Ubuntu"};




    @FXML
    void Resumen(ActionEvent event) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("resumen.fxml"));
        Parent root = loader.load();
        ResumenController controlador = loader.getController();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();

    }




}


