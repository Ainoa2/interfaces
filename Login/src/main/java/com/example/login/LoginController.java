package com.example.login;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.awt.event.MouseEvent;
import java.io.IOException;

public class LoginController {

    @FXML
    private Label bienvenido;

    @FXML
    private PasswordField pantallaPass;

    @FXML
    private TextField pantallaUser;

    @FXML
    void Iniciar(ActionEvent event) throws IOException {

        if (pantallaUser.getText().equals("ainoa") && pantallaPass.getText().equals("1234")) {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("correcto.fxml"));
            Scene scene = new Scene(root, 600, 400);
            stage.setTitle("Login correcto");
            stage.setScene(scene);
            stage.show();
        } else {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("incorrecto.fxml"));
            Scene scene = new Scene(root, 600, 400);
            stage.setTitle("Login incorrecto");
            stage.setScene(scene);
            stage.show();
        }
    }

    public void login(ActionEvent actionEvent) {
    }

    public void calculadora(ActionEvent actionEvent) {
    }

    public void formulario(ActionEvent actionEvent) {
    }
}