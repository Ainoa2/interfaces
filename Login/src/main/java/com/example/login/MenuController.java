package com.example.login;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MenuController {
    @FXML
    void calculadora(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("calculadora.fxml"));
        Scene scene = new Scene(root, 281, 400);
        stage.setTitle("Calculadora");
        stage.setScene(scene);
        stage.show();

    }

    @FXML
    void formulario(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("encuesta.fxml"));
        Scene scene = new Scene(root, 470, 463);
        stage.setTitle("Formulario");
        stage.setScene(scene);
        stage.show();

    }

    @FXML
    void login(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("Login");
        stage.setScene(scene);
        stage.show();

    }

}


