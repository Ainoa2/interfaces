package com.example.actvidad11;

import javafx.application.Application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
       stage.setTitle("Actividad 1.1");

        Rectangle rectangle1 = new Rectangle(50, 50);
        rectangle1.setX(0);
        rectangle1.setY(0);
        rectangle1.setFill(Color.LIGHTSKYBLUE);
        rectangle1.setOnMouseClicked((event->{
         Text text=new Text(20,20,"Has pulsado el 1,1");
         text.setFill(Color.BLACK);
         Group root=new Group(text);
         Scene theScene = new Scene(root, 400, 400);
         stage.setScene(theScene);
         stage.show();
        }));

        Rectangle rectangle2 = new Rectangle(50, 50);
        rectangle2.setX(50);
        rectangle2.setY(0);
        rectangle2.setFill(Color.WHITE);
        rectangle2.setOnMouseClicked((event->{
        Text text=new Text(20,20,"Has pulsado el 1,2");
        text.setFill(Color.BLACK);
        Group root=new Group(text);
        Scene theScene = new Scene(root, 400, 400);
        stage.setScene(theScene);
        stage.show();
        }));

        Rectangle rectangle3 = new Rectangle(50, 50);
        rectangle3.setX(100);
        rectangle3.setY(0);
        rectangle3.setFill(Color.LIGHTSKYBLUE);
        rectangle3.setOnMouseClicked((event->{
        Text text=new Text(20,20,"Has pulsado el 1,3");
        text.setFill(Color.BLACK);
        Group root=new Group(text);
        Scene theScene = new Scene(root, 400, 400);
        stage.setScene(theScene);
        stage.show();
        }));

        Rectangle rectangle4 = new Rectangle(50, 50);
        rectangle4.setX(150);
        rectangle4.setY(0);
        rectangle4.setFill(Color.WHITE);
        rectangle4.setOnMouseClicked((event->{
        Text text=new Text(20,20,"Has pulsado el 1,4");
        text.setFill(Color.BLACK);
        Group root=new Group(text);
        Scene theScene = new Scene(root, 400, 400);
        stage.setScene(theScene);
        stage.show();
        }));

        Rectangle rectangle5 = new Rectangle(50, 50);
        rectangle5.setX(200);
        rectangle5.setY(0);
        rectangle5.setFill(Color.LIGHTSKYBLUE);
     rectangle5.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 1,5");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle6 = new Rectangle(50, 50);
        rectangle6.setX(250);
        rectangle6.setY(0);
        rectangle6.setFill(Color.WHITE);
     rectangle6.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 1,6");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle7 = new Rectangle(50, 50);
        rectangle7.setX(300);
        rectangle7.setY(0);
        rectangle7.setFill(Color.LIGHTSKYBLUE);
     rectangle7.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 1,7");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle8 = new Rectangle(50, 50);
        rectangle8.setX(350);
        rectangle8.setY(0);
        rectangle8.setFill(Color.WHITE);
     rectangle8.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 1,8");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle9 = new Rectangle(50, 50);
        rectangle9.setX(0);
        rectangle9.setY(50);
        rectangle9.setFill(Color.WHITE);
     rectangle9.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 2,1");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle10 = new Rectangle(50, 50);
        rectangle10.setX(50);
        rectangle10.setY(50);
        rectangle10.setFill(Color.LIGHTSKYBLUE);
     rectangle10.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 2,2");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle11 = new Rectangle(50, 50);
        rectangle11.setX(100);
        rectangle11.setY(50);
        rectangle11.setFill(Color.WHITE);
     rectangle11.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 2,3");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle12 = new Rectangle(50, 50);
        rectangle12.setX(150);
        rectangle12.setY(50);
        rectangle12.setFill(Color.LIGHTSKYBLUE);
     rectangle12.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 2,4");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle13 = new Rectangle(50, 50);
        rectangle13.setX(200);
        rectangle13.setY(50);
        rectangle13.setFill(Color.WHITE);
     rectangle13.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 2,5");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle14 = new Rectangle(50, 50);
        rectangle14.setX(250);
        rectangle14.setY(50);
        rectangle14.setFill(Color.LIGHTSKYBLUE);
     rectangle14.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 2,6");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle15 = new Rectangle(50, 50);
        rectangle15.setX(300);
        rectangle15.setY(50);
        rectangle15.setFill(Color.WHITE);
     rectangle15.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 2,7");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle16 = new Rectangle(50, 50);
        rectangle16.setX(350);
        rectangle16.setY(50);
        rectangle16.setFill(Color.LIGHTSKYBLUE);
     rectangle16.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 2,8");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle17 = new Rectangle(50, 50);
        rectangle17.setX(0);
        rectangle17.setY(100);
        rectangle17.setFill(Color.LIGHTSKYBLUE);
     rectangle17.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 3,1");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle18 = new Rectangle(50, 50);
        rectangle18.setX(50);
        rectangle18.setY(100);
        rectangle18.setFill(Color.WHITE);
     rectangle18.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 3,2");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle19 = new Rectangle(50, 50);
        rectangle19.setX(100);
        rectangle19.setY(100);
        rectangle19.setFill(Color.LIGHTSKYBLUE);
     rectangle19.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 3,3");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle20 = new Rectangle(50, 50);
        rectangle20.setX(150);
        rectangle20.setY(100);
        rectangle20.setFill(Color.WHITE);
     rectangle20.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 3,4");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle21 = new Rectangle(50, 50);
        rectangle21.setX(200);
        rectangle21.setY(100);
        rectangle21.setFill(Color.LIGHTSKYBLUE);
     rectangle21.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 3,5");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle22 = new Rectangle(50, 50);
        rectangle22.setX(250);
        rectangle22.setY(100);
        rectangle22.setFill(Color.WHITE);
     rectangle22.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 3,6");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle23 = new Rectangle(50, 50);
        rectangle23.setX(300);
        rectangle23.setY(100);
        rectangle23.setFill(Color.LIGHTSKYBLUE);
     rectangle23.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 3,7");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle24 = new Rectangle(50, 50);
        rectangle24.setX(350);
        rectangle24.setY(100);
        rectangle24.setFill(Color.WHITE);
     rectangle24.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 3,8");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle25 = new Rectangle(50, 50);
        rectangle25.setX(0);
        rectangle25.setY(150);
        rectangle25.setFill(Color.WHITE);
     rectangle25.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 4,1");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle26 = new Rectangle(50, 50);
        rectangle26.setX(50);
        rectangle26.setY(150);
        rectangle26.setFill(Color.LIGHTSKYBLUE);
     rectangle26.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 4,2");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle27 = new Rectangle(50, 50);
        rectangle27.setX(100);
        rectangle27.setY(150);
        rectangle27.setFill(Color.WHITE);
     rectangle27.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 4,3");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle28 = new Rectangle(50, 50);
        rectangle28.setX(150);
        rectangle28.setY(150);
        rectangle28.setFill(Color.LIGHTSKYBLUE);
     rectangle28.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 4,4");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle29 = new Rectangle(50, 50);
        rectangle29.setX(200);
        rectangle29.setY(150);
        rectangle29.setFill(Color.WHITE);
     rectangle29.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 4,5");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle30 = new Rectangle(50, 50);
        rectangle30.setX(250);
        rectangle30.setY(150);
        rectangle30.setFill(Color.LIGHTSKYBLUE);
     rectangle30.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 4,6");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle31 = new Rectangle(50, 50);
        rectangle31.setX(300);
        rectangle31.setY(150);
        rectangle31.setFill(Color.WHITE);
     rectangle31.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 4,7");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle32 = new Rectangle(50, 50);
        rectangle32.setX(350);
        rectangle32.setY(150);
        rectangle32.setFill(Color.LIGHTSKYBLUE);
     rectangle32.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 4,8");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle33 = new Rectangle(50, 50);
        rectangle33.setX(0);
        rectangle33.setY(200);
        rectangle33.setFill(Color.LIGHTSKYBLUE);
     rectangle33.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 5,1");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle34 = new Rectangle(50, 50);
        rectangle34.setX(50);
        rectangle34.setY(200);
        rectangle34.setFill(Color.WHITE);
     rectangle34.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 5,2");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle35 = new Rectangle(50, 50);
        rectangle35.setX(100);
        rectangle35.setY(200);
        rectangle35.setFill(Color.LIGHTSKYBLUE);
     rectangle35.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 5,3");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle36 = new Rectangle(50, 50);
        rectangle36.setX(150);
        rectangle36.setY(200);
        rectangle36.setFill(Color.WHITE);
     rectangle36.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 5,4");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle37 = new Rectangle(50, 50);
        rectangle37.setX(200);
        rectangle37.setY(200);
        rectangle37.setFill(Color.LIGHTSKYBLUE);
     rectangle37.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 5,5");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle38 = new Rectangle(50, 50);
        rectangle38.setX(250);
        rectangle38.setY(200);
        rectangle38.setFill(Color.WHITE);
     rectangle38.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 5,6");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle39 = new Rectangle(50, 50);
        rectangle39.setX(300);
        rectangle39.setY(200);
        rectangle39.setFill(Color.LIGHTSKYBLUE);
     rectangle39.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 5,7");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle40 = new Rectangle(50, 50);
        rectangle40.setX(350);
        rectangle40.setY(200);
        rectangle40.setFill(Color.WHITE);
     rectangle40.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 5,8");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle41 = new Rectangle(50, 50);
        rectangle41.setX(0);
        rectangle41.setY(250);
        rectangle41.setFill(Color.WHITE);
     rectangle41.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 6,1");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle42 = new Rectangle(50, 50);
        rectangle42.setX(50);
        rectangle42.setY(250);
        rectangle42.setFill(Color.LIGHTSKYBLUE);
     rectangle42.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 6,2");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle43 = new Rectangle(50, 50);
        rectangle43.setX(100);
        rectangle43.setY(250);
        rectangle43.setFill(Color.WHITE);
     rectangle43.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 6,3");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle44 = new Rectangle(50, 50);
        rectangle44.setX(150);
        rectangle44.setY(250);
        rectangle44.setFill(Color.LIGHTSKYBLUE);
     rectangle44.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 6,4");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle45 = new Rectangle(50, 50);
        rectangle45.setX(200);
        rectangle45.setY(250);
        rectangle45.setFill(Color.WHITE);
     rectangle45.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 6,5");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle46 = new Rectangle(50, 50);
        rectangle46.setX(250);
        rectangle46.setY(250);
        rectangle46.setFill(Color.LIGHTSKYBLUE);
     rectangle46.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 6,6");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle47 = new Rectangle(50, 50);
        rectangle47.setX(300);
        rectangle47.setY(250);
        rectangle47.setFill(Color.WHITE);
     rectangle47.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 6,7");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle48 = new Rectangle(50, 50);
        rectangle48.setX(350);
        rectangle48.setY(250);
        rectangle48.setFill(Color.LIGHTSKYBLUE);
     rectangle48.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 6,8");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle49 = new Rectangle(50, 50);
        rectangle49.setX(0);
        rectangle49.setY(300);
        rectangle49.setFill(Color.LIGHTSKYBLUE);
     rectangle49.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 7,1");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle50 = new Rectangle(50, 50);
        rectangle50.setX(50);
        rectangle50.setY(300);
        rectangle50.setFill(Color.WHITE);
     rectangle50.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 7,2");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle51 = new Rectangle(50, 50);
        rectangle51.setX(100);
        rectangle51.setY(300);
        rectangle51.setFill(Color.LIGHTSKYBLUE);
     rectangle51.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 7,3");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle52 = new Rectangle(50, 50);
        rectangle52.setX(150);
        rectangle52.setY(300);
        rectangle52.setFill(Color.WHITE);
     rectangle52.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 7,4");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle53 = new Rectangle(50, 50);
        rectangle53.setX(200);
        rectangle53.setY(300);
        rectangle53.setFill(Color.LIGHTSKYBLUE);
     rectangle53.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 7,5");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle54 = new Rectangle(50, 50);
        rectangle54.setX(250);
        rectangle54.setY(300);
        rectangle54.setFill(Color.WHITE);
     rectangle54.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 7,6");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle55 = new Rectangle(50, 50);
        rectangle55.setX(300);
        rectangle55.setY(300);
        rectangle55.setFill(Color.LIGHTSKYBLUE);
     rectangle55.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 7,7");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle56 = new Rectangle(50, 50);
        rectangle56.setX(350);
        rectangle56.setY(300);
        rectangle56.setFill(Color.WHITE);
     rectangle56.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 7,8");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle57 = new Rectangle(50, 50);
        rectangle57.setX(0);
        rectangle57.setY(350);
        rectangle57.setFill(Color.WHITE);
     rectangle57.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 8,1");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle58 = new Rectangle(50, 50);
        rectangle58.setX(50);
        rectangle58.setY(350);
        rectangle58.setFill(Color.LIGHTSKYBLUE);
     rectangle58.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 8,2");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle59 = new Rectangle(50, 50);
        rectangle59.setX(100);
        rectangle59.setY(350);
        rectangle59.setFill(Color.WHITE);
     rectangle59.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 8,3");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle60 = new Rectangle(50, 50);
        rectangle60.setX(150);
        rectangle60.setY(350);
        rectangle60.setFill(Color.LIGHTSKYBLUE);
     rectangle60.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 8,4");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle61 = new Rectangle(50, 50);
        rectangle61.setX(200);
        rectangle61.setY(350);
        rectangle61.setFill(Color.WHITE);
     rectangle61.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 8,5");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle62 = new Rectangle(50, 50);
        rectangle62.setX(250);
        rectangle62.setY(350);
        rectangle62.setFill(Color.LIGHTSKYBLUE);
     rectangle62.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 8,6");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle63 = new Rectangle(50, 50);
        rectangle63.setX(300);
        rectangle63.setY(350);
        rectangle63.setFill(Color.WHITE);
     rectangle63.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 8,7");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));

        Rectangle rectangle64 = new Rectangle(50, 50);
        rectangle64.setX(350);
        rectangle64.setY(350);
        rectangle64.setFill(Color.LIGHTSKYBLUE);
     rectangle64.setOnMouseClicked((event->{
      Text text=new Text(20,20,"Has pulsado el 8,8");
      text.setFill(Color.BLACK);
      Group root=new Group(text);
      Scene theScene = new Scene(root, 400, 400);
      stage.setScene(theScene);
      stage.show();
     }));


        Group root = new Group(rectangle1, rectangle2, rectangle3, rectangle4, rectangle5, rectangle6, rectangle7, rectangle8,
                 rectangle9, rectangle10, rectangle11, rectangle12, rectangle13, rectangle14, rectangle15, rectangle16,
                rectangle17, rectangle18, rectangle19, rectangle20, rectangle21, rectangle22, rectangle23, rectangle24,
                rectangle25, rectangle26, rectangle27, rectangle28, rectangle29, rectangle30, rectangle31, rectangle32,
                rectangle33, rectangle34, rectangle35, rectangle36, rectangle37, rectangle38, rectangle39, rectangle40,
                rectangle41, rectangle42, rectangle43, rectangle44, rectangle45, rectangle46, rectangle47, rectangle48,
                rectangle49, rectangle50, rectangle51, rectangle52, rectangle53, rectangle54, rectangle55, rectangle56,
                rectangle57, rectangle58, rectangle59, rectangle60, rectangle61, rectangle62, rectangle63, rectangle64);


        Scene theScene = new Scene(root, 400, 400);
        stage.setScene(theScene);
        stage.show();


    }

    public static void main(String[] args) {
        launch();
    }
}