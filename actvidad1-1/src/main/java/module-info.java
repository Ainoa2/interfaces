module com.example.actvidad11 {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.bootstrapfx.core;
    requires javafx.graphics;

    opens com.example.actvidad11 to javafx.fxml;
    exports com.example.actvidad11;
}