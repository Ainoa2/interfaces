package com.example.iniciar;

import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

public class CargarController implements Initializable {
    @FXML
    private Circle circulo;

    @FXML
    private Rectangle cuadrado;


    @FXML
    void mover(ActionEvent event) {

            RotateTransition rotate = new RotateTransition();

            rotate.setAxis(Rotate.Z_AXIS);


            rotate.setByAngle(360);


            rotate.setCycleCount(500);


            rotate.setDuration(Duration.millis(1000));


            rotate.setAutoReverse(true);
            rotate.setNode(circulo);


            rotate.play();

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        RotateTransition rotate = new RotateTransition();


        rotate.setAxis(Rotate.Z_AXIS);


        rotate.setByAngle(360);


        rotate.setCycleCount(500);


        rotate.setDuration(Duration.millis(1000));


        rotate.setAutoReverse(true);
        rotate.setNode(cuadrado);


        rotate.play();


        TranslateTransition transT = new TranslateTransition();

        transT.setByX(200);
        transT.setNode(circulo);


        transT.setDuration(Duration.millis(1000));


        transT.setCycleCount(500);


        transT.setAutoReverse(true);


        transT.setNode(circulo);


        transT.play();

    }
}
