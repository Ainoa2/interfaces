package com.example.iniciar;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloController {


    @FXML
    void iniciar(ActionEvent event) throws IOException {

        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("cargar.fxml"));
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("Cargar");
        stage.setScene(scene);
        stage.show();

    }

    public void mover(ActionEvent actionEvent) {
    }
}