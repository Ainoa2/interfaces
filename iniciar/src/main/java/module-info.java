module com.example.iniciar {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.bootstrapfx.core;

    opens com.example.iniciar to javafx.fxml;
    exports com.example.iniciar;
}